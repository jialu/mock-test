class MockTestException < Exception
	def message
		"MockTestException"
	end
end

class MockTest
	@@ver = ['b',0,0,0,1]
	@@changelog=%q{
[b.0.0.01]
2016.01.09
Initial Setup
.

}
	@@curr_case = nil
	@@case_hash = {}
	@@err_stack = []
	@@count_success = 0
	@@count_failed = 0
	@@count_pending = 0

	def self.show(kw=nil)
		puts "MockTest"
		puts "===================="

		case kw
		when :version
			puts "version:" + @@ver.join('.')

		when :changelog
			puts "ChangeLog:"
			puts @@changelog
		else
			puts "Avaliable Parameters"
			puts ""
			puts " :version"
			puts " :changelog"
		end	

		puts "===================="

	end

	def self.activity(case_name, &actions)
		@@err_stack.clear 
		@@count_success = 0
		@@count_failed = 0
		@@count_pending = 0

		@@case_name = case_name.to_sym
		@@curr_case = case_name
		@@case_hash[@@case_hash]

		puts "\033[36m-> Activity: #{case_name}\033[0m"
		if block_given? 
			puts "   [Action]"
			puts "   **********************"
			actions.call 

		end
		puts ""
		if @@err_stack.length != 0 
			puts "   [Error Stack]"
			puts "   ~~~~~~~~~~~~~~~~~~~~~~"
			puts @@err_stack.join("\n")
			puts ""
		end
		puts "   [Summary]"
		puts "   **********************"
		puts "   \033[32mSuccess\033[0m: #{@@count_success}"
		puts "   \033[31mFailed\033[0m:  #{@@count_failed}"
		puts "   \033[33mPending\033[0m: #{@@count_pending}"
		puts ""

	end

	def self.xactivity(case_name, &actions)
		@@case_hash = nil
		puts "\033[33m≡\033[0m  Activity: #{case_name}"
	end

	def self.action(action_name, &action_call)
		if @@curr_case == nil
			puts "   \033[33m≡\033[0m #{action_name}" 
			@@count_pending += 1
			return nil
		end
		begin
			action_call.call if block_given?
			puts "   \033[32m√\033[0m #{action_name}"
			@@count_success += 1
		rescue Exception => e
			puts "   \033[31m×\033[0m #{action_name}"
			@@count_failed += 1

			err "\033[41m-> Exception: #{action_name}\033[0m"
			err "┗>#{e.inspect}"
			err " ┗>#{e.backtrace.join("\n      ")}"
		end
	end

	def self.xaction(action_name, &action_call)
		puts "   \033[33m≡\033[0m #{action_name}" 
		@@count_pending += 1
		return nil
	end

	def self.err(line)
		@@err_stack << "   #{line}"
	end

	Kernel.class_eval{
		def eq?(value)
			raise MockTestException, "\033[31mMock: Eq evalution failed\033[0m " if self != value
		end

		def not_eq?(value)
			raise MockTestException, "\033[31mMock: Not_Eq evalution failed\033[0m " if self == value
		end
	}
end


class MyTest < MockTest
	xactivity 'FirstCase' do 
		xaction 'Data Check I' do
			'sdf'.eq? 'sdf'
		end

		xaction 'Data Check II' do
			
		end

		xaction 'Data Check III' do
			'sdf'.not_eq? 'sdf'
		end
	end

	xactivity 'SecondCase' do 
		xaction 'Loc Check I' do
			'sdf'.eq? 123
		end

		xaction 'Loc Check II' do
			
		end

		xaction 'Loc Check III' do
			'sdf'.not_eq? '123'
		end
	end
end





