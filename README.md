# MockTest
A Compact Test Tool. 

<br>

### Usage:

Activity(Root)
------------------
+ activity  
+ xactivity 

<pre><code>activity 'FirstCase' do 
	actions...
end
</code></pre>

Action(Item)
------------------
+ action
+ xaction

<pre><code>activity 'FirstCase' do 
	action 'Data Check I' do
		action body
	end
end
</code></pre>

Result
------------------
+ √&nbsp;&nbsp;&nbsp;=> Action Success
+ ×&nbsp;&nbsp;&nbsp;=> Action Failed
+ ≡&nbsp;&nbsp;&nbsp;=> Action Pending

### Example:

> vi mock_test.rb 

<pre><code>class MyTest &lt; MockTest
	activity 'FirstCase' do 
		action 'Data Check I' do
			'sdf'.eq? 'sdf'
		end

		xaction 'Data Check II' do
			#skip this action
		end

		action 'Data Check III' do
			'sdf'.not_eq? 'sdf'
		end
	end
end
</code></pre>

> ruby mock_test.rb 

<pre><code> 
-> Activity: FirstCase
   [Action]
   **********************
   √ Data Check I
   ≡ Data Check II
   × Data Check III

   [Error Stack]
   ~~~~~~~~~~~~~~~~~~~~~~
   -> Exception: Data Check III
   ┗>#&lt;MockTestException: Mock: Not_Eq evalution failed &gt;
    ┗>mock_test.rb:119:in `not_eq?'
      mock_test.rb:136:in `block (2 levels) in &lt;class:MyTest&gt;'
      mock_test.rb:90:in `call'
      mock_test.rb:90:in `action'
      mock_test.rb:135:in `block in &lt;class:MyTest&gt;'
      mock_test.rb:59:in `call'
      mock_test.rb:59:in `activity'
      mock_test.rb:126:in `&lt;class:MyTest&gt;'
      mock_test.rb:125:in `&lt;main&gt;'

   [Summary]
   **********************
   Success: 1
   Failed:  1
   Pending: 1
</code></pre>